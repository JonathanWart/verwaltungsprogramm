﻿namespace verwaltungsProgramm
{
    partial class FormVerwaltungsprogramm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSpeichern = new System.Windows.Forms.Button();
            this.buttonLoeschen = new System.Windows.Forms.Button();
            this.buttonAendern = new System.Windows.Forms.Button();
            this.buttonZurueck = new System.Windows.Forms.Button();
            this.buttonWeiter = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.buttonAnAnfang = new System.Windows.Forms.Button();
            this.buttonAnEnde = new System.Windows.Forms.Button();
            this.lblKundennr = new System.Windows.Forms.Label();
            this.lblVorname = new System.Windows.Forms.Label();
            this.lblNachname = new System.Windows.Forms.Label();
            this.lblTelnr = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxKundennr = new System.Windows.Forms.TextBox();
            this.txtBoxVorname = new System.Windows.Forms.TextBox();
            this.txtBoxNachname = new System.Windows.Forms.TextBox();
            this.txtBoxTelNr = new System.Windows.Forms.TextBox();
            this.txtBoxHausnr = new System.Windows.Forms.TextBox();
            this.lblHausnr = new System.Windows.Forms.Label();
            this.buttonSpeichernAlsDatei = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemoeffnen = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAlsDateiSpeichern = new System.Windows.Forms.ToolStripMenuItem();
            this.inDer50erZone180FahrenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bordsteineUndPassantenGefährdenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSpeichern
            // 
            this.buttonSpeichern.Location = new System.Drawing.Point(705, 786);
            this.buttonSpeichern.Name = "buttonSpeichern";
            this.buttonSpeichern.Size = new System.Drawing.Size(225, 69);
            this.buttonSpeichern.TabIndex = 0;
            this.buttonSpeichern.TabStop = false;
            this.buttonSpeichern.Text = "Speichern";
            this.buttonSpeichern.UseVisualStyleBackColor = true;
            this.buttonSpeichern.Click += new System.EventHandler(this.buttonSpeichern_Click);
            // 
            // buttonLoeschen
            // 
            this.buttonLoeschen.Location = new System.Drawing.Point(963, 786);
            this.buttonLoeschen.Name = "buttonLoeschen";
            this.buttonLoeschen.Size = new System.Drawing.Size(225, 69);
            this.buttonLoeschen.TabIndex = 1;
            this.buttonLoeschen.Text = "Löschen";
            this.buttonLoeschen.UseVisualStyleBackColor = true;
            // 
            // buttonAendern
            // 
            this.buttonAendern.Location = new System.Drawing.Point(447, 786);
            this.buttonAendern.Name = "buttonAendern";
            this.buttonAendern.Size = new System.Drawing.Size(225, 69);
            this.buttonAendern.TabIndex = 2;
            this.buttonAendern.Text = "Ändern";
            this.buttonAendern.UseVisualStyleBackColor = true;
            // 
            // buttonZurueck
            // 
            this.buttonZurueck.Location = new System.Drawing.Point(313, 786);
            this.buttonZurueck.Name = "buttonZurueck";
            this.buttonZurueck.Size = new System.Drawing.Size(100, 69);
            this.buttonZurueck.TabIndex = 3;
            this.buttonZurueck.Text = "<";
            this.buttonZurueck.UseVisualStyleBackColor = true;
            this.buttonZurueck.Click += new System.EventHandler(this.buttonZurueck_Click);
            // 
            // buttonWeiter
            // 
            this.buttonWeiter.Location = new System.Drawing.Point(1231, 786);
            this.buttonWeiter.Name = "buttonWeiter";
            this.buttonWeiter.Size = new System.Drawing.Size(100, 69);
            this.buttonWeiter.TabIndex = 4;
            this.buttonWeiter.Text = ">";
            this.buttonWeiter.UseVisualStyleBackColor = true;
            this.buttonWeiter.Click += new System.EventHandler(this.buttonWeiter_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(198, 66);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(0, 0);
            this.button6.TabIndex = 1;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(112, 132);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(0, 0);
            this.button7.TabIndex = 0;
            // 
            // buttonAnAnfang
            // 
            this.buttonAnAnfang.Location = new System.Drawing.Point(179, 786);
            this.buttonAnAnfang.Name = "buttonAnAnfang";
            this.buttonAnAnfang.Size = new System.Drawing.Size(100, 69);
            this.buttonAnAnfang.TabIndex = 5;
            this.buttonAnAnfang.Text = "I<";
            this.buttonAnAnfang.UseVisualStyleBackColor = true;
            this.buttonAnAnfang.Click += new System.EventHandler(this.buttonAnAnfang_Click);
            // 
            // buttonAnEnde
            // 
            this.buttonAnEnde.Location = new System.Drawing.Point(1385, 786);
            this.buttonAnEnde.Name = "buttonAnEnde";
            this.buttonAnEnde.Size = new System.Drawing.Size(100, 69);
            this.buttonAnEnde.TabIndex = 6;
            this.buttonAnEnde.Text = ">I";
            this.buttonAnEnde.UseVisualStyleBackColor = true;
            this.buttonAnEnde.Click += new System.EventHandler(this.buttonAnEnde_Click);
            // 
            // lblKundennr
            // 
            this.lblKundennr.AutoSize = true;
            this.lblKundennr.Location = new System.Drawing.Point(182, 116);
            this.lblKundennr.Name = "lblKundennr";
            this.lblKundennr.Size = new System.Drawing.Size(275, 48);
            this.lblKundennr.TabIndex = 7;
            this.lblKundennr.Text = "Kundennummer";
            // 
            // lblVorname
            // 
            this.lblVorname.AutoSize = true;
            this.lblVorname.Location = new System.Drawing.Point(182, 220);
            this.lblVorname.Name = "lblVorname";
            this.lblVorname.Size = new System.Drawing.Size(162, 48);
            this.lblVorname.TabIndex = 8;
            this.lblVorname.Text = "Vorname";
            // 
            // lblNachname
            // 
            this.lblNachname.AutoSize = true;
            this.lblNachname.Location = new System.Drawing.Point(182, 324);
            this.lblNachname.Name = "lblNachname";
            this.lblNachname.Size = new System.Drawing.Size(190, 48);
            this.lblNachname.TabIndex = 9;
            this.lblNachname.Text = "Nachname";
            // 
            // lblTelnr
            // 
            this.lblTelnr.AutoSize = true;
            this.lblTelnr.Location = new System.Drawing.Point(182, 430);
            this.lblTelnr.Name = "lblTelnr";
            this.lblTelnr.Size = new System.Drawing.Size(268, 48);
            this.lblTelnr.TabIndex = 10;
            this.lblTelnr.Text = "Telefonnummer";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(174, 510);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 48);
            this.label5.TabIndex = 11;
            // 
            // txtBoxKundennr
            // 
            this.txtBoxKundennr.Location = new System.Drawing.Point(532, 113);
            this.txtBoxKundennr.Name = "txtBoxKundennr";
            this.txtBoxKundennr.Size = new System.Drawing.Size(300, 55);
            this.txtBoxKundennr.TabIndex = 12;
            // 
            // txtBoxVorname
            // 
            this.txtBoxVorname.Location = new System.Drawing.Point(532, 217);
            this.txtBoxVorname.Name = "txtBoxVorname";
            this.txtBoxVorname.Size = new System.Drawing.Size(300, 55);
            this.txtBoxVorname.TabIndex = 13;
            // 
            // txtBoxNachname
            // 
            this.txtBoxNachname.Location = new System.Drawing.Point(532, 321);
            this.txtBoxNachname.Name = "txtBoxNachname";
            this.txtBoxNachname.Size = new System.Drawing.Size(300, 55);
            this.txtBoxNachname.TabIndex = 14;
            // 
            // txtBoxTelNr
            // 
            this.txtBoxTelNr.Location = new System.Drawing.Point(532, 427);
            this.txtBoxTelNr.Name = "txtBoxTelNr";
            this.txtBoxTelNr.Size = new System.Drawing.Size(300, 55);
            this.txtBoxTelNr.TabIndex = 15;
            // 
            // txtBoxHausnr
            // 
            this.txtBoxHausnr.Location = new System.Drawing.Point(532, 526);
            this.txtBoxHausnr.Name = "txtBoxHausnr";
            this.txtBoxHausnr.Size = new System.Drawing.Size(300, 55);
            this.txtBoxHausnr.TabIndex = 16;
            // 
            // lblHausnr
            // 
            this.lblHausnr.AutoSize = true;
            this.lblHausnr.Location = new System.Drawing.Point(182, 529);
            this.lblHausnr.Name = "lblHausnr";
            this.lblHausnr.Size = new System.Drawing.Size(233, 48);
            this.lblHausnr.TabIndex = 17;
            this.lblHausnr.Text = "Hausnummer";
            // 
            // buttonSpeichernAlsDatei
            // 
            this.buttonSpeichernAlsDatei.Location = new System.Drawing.Point(447, 891);
            this.buttonSpeichernAlsDatei.Name = "buttonSpeichernAlsDatei";
            this.buttonSpeichernAlsDatei.Size = new System.Drawing.Size(483, 67);
            this.buttonSpeichernAlsDatei.TabIndex = 18;
            this.buttonSpeichernAlsDatei.Text = "Als Datei speichern";
            this.buttonSpeichernAlsDatei.UseVisualStyleBackColor = true;
            this.buttonSpeichernAlsDatei.Click += new System.EventHandler(this.BttnSpeichernAlsDatei_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1646, 56);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemoeffnen,
            this.ToolStripMenuItemAlsDateiSpeichern,
            this.inDer50erZone180FahrenToolStripMenuItem,
            this.bordsteineUndPassantenGefährdenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(131, 52);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // ToolStripMenuItemoeffnen
            // 
            this.ToolStripMenuItemoeffnen.Name = "ToolStripMenuItemoeffnen";
            this.ToolStripMenuItemoeffnen.Size = new System.Drawing.Size(538, 66);
            this.ToolStripMenuItemoeffnen.Text = "Öffnen...";
            // 
            // ToolStripMenuItemAlsDateiSpeichern
            // 
            this.ToolStripMenuItemAlsDateiSpeichern.Name = "ToolStripMenuItemAlsDateiSpeichern";
            this.ToolStripMenuItemAlsDateiSpeichern.Size = new System.Drawing.Size(538, 66);
            this.ToolStripMenuItemAlsDateiSpeichern.Text = "Speichern unter...";
            this.ToolStripMenuItemAlsDateiSpeichern.Click += new System.EventHandler(this.alsDateiSpeichernToolStripMenuItem_Click);
            // 
            // inDer50erZone180FahrenToolStripMenuItem
            // 
            this.inDer50erZone180FahrenToolStripMenuItem.Name = "inDer50erZone180FahrenToolStripMenuItem";
            this.inDer50erZone180FahrenToolStripMenuItem.Size = new System.Drawing.Size(797, 66);
            this.inDer50erZone180FahrenToolStripMenuItem.Text = "In der 50er Zone 180 fahren...";
            // 
            // bordsteineUndPassantenGefährdenToolStripMenuItem
            // 
            this.bordsteineUndPassantenGefährdenToolStripMenuItem.Name = "bordsteineUndPassantenGefährdenToolStripMenuItem";
            this.bordsteineUndPassantenGefährdenToolStripMenuItem.Size = new System.Drawing.Size(797, 66);
            this.bordsteineUndPassantenGefährdenToolStripMenuItem.Text = "Bordsteine und Passanten gefährden";
            // 
            // FormVerwaltungsprogramm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(20F, 48F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1646, 1218);
            this.Controls.Add(this.buttonSpeichernAlsDatei);
            this.Controls.Add(this.lblHausnr);
            this.Controls.Add(this.txtBoxHausnr);
            this.Controls.Add(this.txtBoxTelNr);
            this.Controls.Add(this.txtBoxNachname);
            this.Controls.Add(this.txtBoxVorname);
            this.Controls.Add(this.txtBoxKundennr);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblTelnr);
            this.Controls.Add(this.lblNachname);
            this.Controls.Add(this.lblVorname);
            this.Controls.Add(this.lblKundennr);
            this.Controls.Add(this.buttonAnEnde);
            this.Controls.Add(this.buttonAnAnfang);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.buttonWeiter);
            this.Controls.Add(this.buttonZurueck);
            this.Controls.Add(this.buttonAendern);
            this.Controls.Add(this.buttonLoeschen);
            this.Controls.Add(this.buttonSpeichern);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormVerwaltungsprogramm";
            this.Text = "Verwaltungsprogramm";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button buttonSpeichern;
        private Button buttonLoeschen;
        private Button buttonAendern;
        private Button buttonZurueck;
        private Button buttonWeiter;
        private Button button6;
        private Button button7;
        private Button buttonAnAnfang;
        private Button buttonAnEnde;
        private Label lblKundennr;
        private Label lblVorname;
        private Label lblNachname;
        private Label lblTelnr;
        private Label label5;
        private TextBox txtBoxKundennr;
        private TextBox txtBoxVorname;
        private TextBox txtBoxNachname;
        private TextBox txtBoxTelNr;
        private TextBox txtBoxHausnr;
        private Label lblHausnr;
        private Button buttonSpeichernAlsDatei;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem dateiToolStripMenuItem;
        private ToolStripMenuItem ToolStripMenuItemoeffnen;
        private ToolStripMenuItem ToolStripMenuItemAlsDateiSpeichern;
        private ToolStripMenuItem inDer50erZone180FahrenToolStripMenuItem;
        private ToolStripMenuItem bordsteineUndPassantenGefährdenToolStripMenuItem;
    }
}