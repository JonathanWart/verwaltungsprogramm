namespace verwaltungsProgramm
{
    struct Kundendaten
    {
        public string kundenNummer;
        public string vorName;
        public string nachName;
        public string telefonNummer;
        public string hausNummer;
    }

    public partial class FormVerwaltungsprogramm : Form
    {
        Kundendaten[] kundenArray;
        int aktuelleArrayPosition;

        public FormVerwaltungsprogramm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            kundenArray = new Kundendaten[10];
            aktuelleArrayPosition = 0;
            buttonZurueck.Enabled = false;
            buttonAnAnfang.Enabled = false;
        }

        private void buttonSpeichern_Click(object sender, EventArgs e)
        {
            Kundendaten kundenDaten;
            kundenDaten.kundenNummer = txtBoxKundennr.Text;
            kundenDaten.vorName = txtBoxVorname.Text;
            kundenDaten.nachName = txtBoxNachname.Text;
            kundenDaten.telefonNummer = txtBoxTelNr.Text;
            kundenDaten.hausNummer = txtBoxHausnr.Text;

            kundenArray[aktuelleArrayPosition] = kundenDaten;
        }

        private void buttonWeiter_Click(object sender, EventArgs e)
        {
            aktuelleArrayPosition++;
            Kundendaten kundenDaten = kundenArray[aktuelleArrayPosition];

            txtBoxKundennr.Text = kundenDaten.kundenNummer;
            txtBoxVorname.Text = kundenDaten.vorName;
            txtBoxNachname.Text = kundenDaten.nachName;
            txtBoxTelNr.Text = kundenDaten.telefonNummer;
            txtBoxHausnr.Text = kundenDaten.hausNummer;

            if (aktuelleArrayPosition >= kundenArray.Length - 1)
            {
                buttonWeiter.Enabled = false;
                buttonAnEnde.Enabled = false;
            }
            else 
            { 
                buttonZurueck.Enabled = true; 
                buttonAnAnfang.Enabled = true;
            }
        }

        private void buttonZurueck_Click(object sender, EventArgs e)
        {
            aktuelleArrayPosition--;
            Kundendaten kundenDaten = kundenArray[aktuelleArrayPosition];

            txtBoxKundennr.Text = kundenDaten.kundenNummer;
            txtBoxVorname.Text = kundenDaten.vorName;
            txtBoxNachname.Text = kundenDaten.nachName;
            txtBoxTelNr.Text = kundenDaten.telefonNummer;
            txtBoxHausnr.Text = kundenDaten.hausNummer;

            if (aktuelleArrayPosition <= 0)
            {
                buttonZurueck.Enabled = false;
                buttonAnAnfang.Enabled = false;
            }
            else
            {
                buttonWeiter.Enabled = true;
                buttonAnEnde.Enabled = true;
            }
        }

        private void buttonAnEnde_Click(object sender, EventArgs e)
        {
            aktuelleArrayPosition = kundenArray.Length - 1;
            Kundendaten kundenDaten = kundenArray[aktuelleArrayPosition];

            txtBoxKundennr.Text = kundenDaten.kundenNummer;
            txtBoxVorname.Text = kundenDaten.vorName;
            txtBoxNachname.Text = kundenDaten.nachName;
            txtBoxTelNr.Text = kundenDaten.telefonNummer;
            txtBoxHausnr.Text = kundenDaten.hausNummer;

            buttonWeiter.Enabled = false;
            buttonZurueck.Enabled= true;
            buttonAnEnde.Enabled= false;
            buttonAnAnfang.Enabled= true;
        }

        private void buttonAnAnfang_Click(object sender, EventArgs e)
        {
            aktuelleArrayPosition = 0;
            Kundendaten kundenDaten = kundenArray[aktuelleArrayPosition];

            txtBoxKundennr.Text = kundenDaten.kundenNummer;
            txtBoxVorname.Text = kundenDaten.vorName;
            txtBoxNachname.Text = kundenDaten.nachName;
            txtBoxTelNr.Text = kundenDaten.telefonNummer;
            txtBoxHausnr.Text = kundenDaten.hausNummer;

            buttonWeiter.Enabled = true;
            buttonZurueck.Enabled = false;
            buttonAnEnde.Enabled = true;
            buttonAnAnfang.Enabled = false;
        }

        private void BttnSpeichernAlsDatei_Click(object sender, EventArgs e)
        {
            int z;
            System.IO.StreamWriter meineDatei;
            meineDatei = new System.IO.StreamWriter("kunden.txt");
            meineDatei.WriteLine(letzter);
            for (z = 0; z < letzter; z++)
            {
                meineDatei.WriteLine(kundenArray[z].kundenNummer);
                meineDatei.WriteLine(kundenArray[z].vorName);
                meineDatei.WriteLine(kundenArray[z].nachName);
                meineDatei.WriteLine(kundenArray[z].telefonNummer);
                meineDatei.WriteLine(kundenArray[z].hausNummer);
            }
            meineDatei.Close();
        }

        private void alsDateiSpeichernToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}